class CreditCardPayment implements PaymentStrategy {
    @Override
    public void pay(int amount) {
        System.out.println("Kredit kartı ilə ödənilir: " + amount);
    }
}